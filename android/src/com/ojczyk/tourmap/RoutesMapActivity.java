package com.ojczyk.tourmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ojczyk.tourmap.RoutesPoiListActivity.LoadPois;
import com.ojczyk.tourmap.helper.AlertDialogManager;
import com.ojczyk.tourmap.helper.ConnectionDetector;
import com.ojczyk.tourmap.helper.JSONParser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.widget.SimpleAdapter;

public class RoutesMapActivity extends FragmentActivity {
	
	private GoogleMap googleMap;
    private int mapType = GoogleMap.MAP_TYPE_NORMAL;
    
    String idRoute;
    Integer intIdRoute;
    
    
    private static final String URL_POIS = "http://ojczyk.nstrefa.pl/tour_map/routesPoiList.php";
    
    // ALL JSON node names
  	private static final String TAG_POIS = "pois";
  	private static final String TAG_ID = "id";
  	private static final String TAG_POI_NAME = "name";
  	private static final String TAG_POI_LAT = "lat";
  	private static final String TAG_POI_LNG = "lng";
  	private static final String TAG_ROUTE_NAME = "route_name";
  	
  	
    // Connection detector
 	ConnectionDetector cd;
 	// Alert dialog manager
 	AlertDialogManager alert = new AlertDialogManager();
 	// Creating JSON Parser object
 	JSONParser jsonParser = new JSONParser();
 	
 	ArrayList<HashMap<String, String>> poiList;
 	
 	String route_id, route_name;
 	
 // POIS JSONArray
 	JSONArray pois = null;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_activity);
		
//POBRANIE MAPY DO ZMIENNEJ
		
		FragmentManager fragmentManager = getSupportFragmentManager();
        SupportMapFragment mapFragment =  (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        googleMap = mapFragment.getMap();
        
        // HASHMA DLA LISTVIEW
        poiList = new ArrayList<HashMap<String, String>>();
        
        cd = new ConnectionDetector(getApplicationContext());
		 
		// SPRAWDZAMY PO��CZENIE INTERNETOWE
        if (!cd.isConnectingToInternet()) {
        	//NIE UDA�O PO��CZYC SI� Z INTERNETEM
            alert.showAlertDialog(RoutesMapActivity.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }
        
        // POBIERAMY ID TRASY Z POSTA
        //Intent i = getIntent();
        //idRoute = i.getStringExtra("id_route");
        idRoute = "1";
        
        // �ADUJEMY POI W TLE
 		new LoadPois().execute();
	}
	
	//POBIERAMY LIST� POI I UAKTUALNIAMY LISTVIEW
	
	
	/**
	 * Background Async Task to Load all tracks under one album
	 * */
	class LoadPois extends AsyncTask<String, String, String> {
		
	
		/**
		 * POBIERAMY JSON POI I PARSUJEMy
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			
			// post route id as GET parameter
			params.add(new BasicNameValuePair(TAG_ID, idRoute));
	
			// getting JSON string from URL
			String json = jsonParser.makeHttpRequest(URL_POIS, "GET", params);
	
			try {
				JSONObject jObj = new JSONObject(json);
				
				if (jObj != null) {
					//POBIERAMY WSZYSTKIE POI DLA TRASY
					pois = jObj.getJSONArray(TAG_POIS);
					route_name = jObj.getString(TAG_ROUTE_NAME);
					
					//setTitle(route_name);
					
					if (pois != null) {
						
						//DLA WSZYSTKICH POBRANYCH POI
						for (int i = 0; i < pois.length(); i++) {
							
							JSONObject c = pois.getJSONObject(i);
	
							String name = c.getString(TAG_POI_NAME);
							String lat = c.getString(TAG_POI_LAT);
							String lng = c.getString(TAG_POI_LNG);
					        
					        // creating new HashMap
							HashMap<String, String> map = new HashMap<String, String>();
	
							// adding each child node to HashMap key => value
							map.put(TAG_POI_NAME, name);
							map.put(TAG_POI_LAT, lng);
							map.put(TAG_POI_LNG, lng);
					        //alert.showAlertDialog(RoutesMapActivity.this, "Internet Connection Error", "name", false);
							//DODAJEMY PUNKTY NA MAPE
							
							poiList.add(map);
						}
					} else {
						
					}
				}
	
			} catch (JSONException e) {
				e.printStackTrace();
			}
	
			return null;
		}
		
		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				
				public void run() {
					setTitle(route_name);
					googleMap.setMapType(mapType);
					
					
					//holder.name.setText();
					for (int i = 0; i < poiList.size(); i++) {
						
						String name = poiList.get(i).get(TAG_POI_NAME);
						String lat = poiList.get(i).get(TAG_POI_LAT);
						String lng = poiList.get(i).get(TAG_POI_LNG);
						
						double latDouble = Double.parseDouble(lat);
						double lngDouble = Double.parseDouble(lng);
						
						addMarker(latDouble, lngDouble, name, "Opis");
					}
				}
				
			});
	
		}
}
	
	
	//DODANIE MARKERA
	public void addMarker(double lat, double lng, String name, String content){
		float cameraZoom = 10;
        //WSPӣRZ�DNE
        LatLng sfLatLng = new LatLng(lat, lng);
        //OPCJE MARKERA
        googleMap.addMarker(
        		new MarkerOptions()
                .position(sfLatLng)
                .title(name)
                .snippet(content)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        
        //WYKONUJEMY AKCJ�
        
        //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sfLatLng, cameraZoom));
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}














