package com.ojczyk.tourmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.ojczyk.tourmap.helper.AlertDialogManager;
import com.ojczyk.tourmap.helper.ConnectionDetector;
import com.ojczyk.tourmap.helper.JSONParser;

public class RoutesActivity extends ListActivity {
	
	
	
//PARAMETRY JSON I PO��CZENIA
	
	// Connection detector
	ConnectionDetector cd;
	
	// Alert dialog manager
	AlertDialogManager alert = new AlertDialogManager();
	
	// Progress Dialog
	private ProgressDialog pDialog;

	// Creating JSON Parser object
	JSONParser jsonParser = new JSONParser();

	ArrayList<HashMap<String, String>> routesList;

	// ROUTES JSONArray
	JSONArray routes = null;

	
//FILTROWANIE LISTY
	//ADAPTER DO WYSZUKIWANIA I ZA�ADOWANIA LISTY
	SimpleAdapter adapter;
	// POLE SEARCH EditText
    EditText inputSearch;
	
	
	// ROUTES JSON url
	private static final String URL_ROUTES = "http://ojczyk.nstrefa.pl/tour_map/routes.php";

	// ALL JSON node names
	private static final String TAG_ID = "id";
	private static final String TAG_NAME = "name";
	private static final String TAG_POI_COUNT = "poi_count";
	private static final String LISTA_TRAS = "Lista tras";
	
	
	
	
	
//PODCZAS TWORZENIA

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//USTAWIAMY LAYER
		setContentView(R.layout.activity_routes);
		
		
		
//DETEKTOR PO��CZENIA Z INTERNETEM
		cd = new ConnectionDetector(getApplicationContext());
		 
        // SPRAWDZAMY PO��CZENIE INTERNETOWE
        if (!cd.isConnectingToInternet()) {
            //NIE UDA�O PO��CZYC SI� Z INTERNETEM
            alert.showAlertDialog(RoutesActivity.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }
        
        
		// HASHMAP DLA LISTVIEW
        routesList = new ArrayList<HashMap<String, String>>();

		// �ADUJEMY ALBUMY Z JSON
		new LoadAlbums().execute();
		
		// POBIERAMY LISTVIEW
		ListView lv = getListView();
		
		
		
		
		
		
		
//FILTROWANIE LISSTVIEW
	    inputSearch = (EditText) findViewById(R.id.inputSearch);
	    inputSearch.setHint("znajd� tras�...");
	    
		inputSearch.addTextChangedListener(new TextWatcher() {
		     
		    @Override
		    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
		        // When user changed the Text
		    	RoutesActivity.this.adapter.getFilter().filter(cs);
		    }
		     
		    @Override
		    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		        // TODO Auto-generated method stub
		    }
		     
		    @Override
		    public void afterTextChanged(Editable arg0) {
		        // TODO Auto-generated method stub                         
		    }
		});
		
		
		
		
		
		
		/**
		 * LISTVIEW ITEM CLICK LISTENER
		 * poiListActivity ZOSTANIE WCZYTANE POPRZEZ PRZES�ANIE ID TRASY
		 * */
		lv.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
				
				//GDY WYBIERZEMY POJEDY�CZ� TRAS� ZOSTANIE WCZYTANA LISTA POI
				Intent i = new Intent(getApplicationContext(), RoutesPoiListActivity.class);
				
				//WYSY�AMY ID TRASY DO LISTY POI ABY UZYSKAC LIST� POI DLA TEJ TRASY
				String album_id = ((TextView) view.findViewById(R.id.route_id)).getText().toString();
				i.putExtra("route_id", album_id);				
				
				startActivity(i);
			}
		});		
	}
	

	/**
	 * �ADUJEMY WSZYSTKIE TRASY POPRZES HTTP REQUEST
	 * */
	class LoadAlbums extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(RoutesActivity.this);
			pDialog.setMessage("Pobieram trasy ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * POBIERAMY JSON TRAS
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			// getting JSON string from URL
			String json = jsonParser.makeHttpRequest(URL_ROUTES, "GET", params);

			try {				
				routes = new JSONArray(json);
				
				if (routes != null) {
					// looping through All routes
					for (int i = 0; i < routes.length(); i++) {
						
						JSONObject c = routes.getJSONObject(i);

						// Storing each json item values in variable
						String id = c.getString(TAG_ID);
						String name = c.getString(TAG_NAME);
						String pois_count = c.getString(TAG_POI_COUNT);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_ID, id);
						map.put(TAG_NAME, name);
						map.put(TAG_POI_COUNT, pois_count);

						// adding HashList to ArrayList
						routesList.add(map);
					}
				}else{
					//Log.d("Routes: ", "null");
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}
		
		
		
//UAKTUALNIAMY LIST� TRAS

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all albums
			pDialog.dismiss();
			
			// UPDATE UI
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * UPDATE LISTVIEW DANYMI Z JSON
					 * */
					RoutesActivity.this.adapter = new SimpleAdapter(RoutesActivity.this, routesList, R.layout.list_item_routes, 
							new String[] 
							{ TAG_ID,TAG_NAME, TAG_POI_COUNT }, 
							new int[] 
							{R.id.route_id, R.id.route_name, R.id.pois_count });
					
					// UPDATE LISTVIEW
					setListAdapter(adapter);
					
					setTitle(LISTA_TRAS);
				}
			});

		}

	}
}