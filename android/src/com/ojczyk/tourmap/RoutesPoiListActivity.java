package com.ojczyk.tourmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ojczyk.tourmap.helper.AlertDialogManager;
import com.ojczyk.tourmap.helper.ConnectionDetector;
import com.ojczyk.tourmap.helper.JSONParser;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.EditText;


public class RoutesPoiListActivity extends ListActivity {
	
//PARAMETRY JSON I PO��CZENIA
	
	// Connection detector
	ConnectionDetector cd;
	
	// Alert dialog manager
	AlertDialogManager alert = new AlertDialogManager();
	
	// Progress Dialog
	private ProgressDialog pDialog;

	// Creating JSON Parser object
	JSONParser jsonParser = new JSONParser();

	ArrayList<HashMap<String, String>> poiList;

	// POIS JSONArray
	JSONArray pois = null;
	
	// id
	String route_id, route_name;
	
	
//FILTROWANIE LISTY
	//ADAPTER DO WYSZUKIWANIA I ZA�ADOWANIA LISTY
	SimpleAdapter adapter;
	// POLE SEARCH EditText
    EditText inputSearch;
	

	// POI JSON URL
	// id - should be posted as GET params to get POI list (ex: id = 5)
	private static final String URL_POIS = "http://ojczyk.nstrefa.pl/tour_map/routesPoiList.php";

	// ALL JSON node names
	private static final String TAG_POIS = "pois";
	private static final String TAG_ID = "id";
	private static final String TAG_POI_NAME = "name";
	private static final String TAG_ROUTE_NAME = "route_name";

	
	
	
//PODCZAS TWORZENIA
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pois);
		
		
//DETEKTOR PO��CZENIA Z INTERNETEM
		cd = new ConnectionDetector(getApplicationContext());
		 
		// SPRAWDZAMY PO��CZENIE INTERNETOWE
        if (!cd.isConnectingToInternet()) {
        	//NIE UDA�O PO��CZYC SI� Z INTERNETEM
            alert.showAlertDialog(RoutesPoiListActivity.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }
        
        
        // POBIERAMY ID TRASY Z POSTA
        Intent i = getIntent();
        route_id = i.getStringExtra("route_id");
        
		// HASHMA DLA LISTVIEW
        poiList = new ArrayList<HashMap<String, String>>();

		// �ADUJEMY POI W TLE
		new LoadPois().execute();
		
		// POBIERAMY LISTVIEW
		ListView lv = getListView();
		
		
		
//FILTROWANIE LISSTVIEW
	    inputSearch = (EditText) findViewById(R.id.inputSearch);
	    inputSearch.setHint("znajd� POI...");
	    
		inputSearch.addTextChangedListener(new TextWatcher() {
		     
		    @Override
		    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
		        // When user changed the Text
		    	RoutesPoiListActivity.this.adapter.getFilter().filter(cs);
		    }
		     
		    @Override
		    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		        // TODO Auto-generated method stub
		    }
		     
		    @Override
		    public void afterTextChanged(Editable arg0) {
		        // TODO Auto-generated method stub                         
		    }
		});
		
		
//OBS�UGUJEMY KLIKNI�CIE W LIST�
		
		/**
		 * LISTVIEW ITEM CLICK LISTENER
		 * RoutesSinglePoiActivity ZOSTANIE WCZYTANE POPRZEZ PRZES�ANIE ID TRASY
		 * */
		lv.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2,long arg3) {
				
				// On selecting single track get song information
				Intent i = new Intent(getApplicationContext(), RoutesSinglePoiActivity.class);
				
				// to get song information
				// both album id and song is needed
				String album_id = ((TextView) view.findViewById(R.id.route_id)).getText().toString();
				String song_id = ((TextView) view.findViewById(R.id.poi_id)).getText().toString();
				
				//Toast.makeText(getApplicationContext(), "Album Id: " + album_id  + ", Song Id: " + song_id, Toast.LENGTH_SHORT).show();
				
				i.putExtra("route_id", album_id);
				i.putExtra("poi_id", song_id);
				
				startActivity(i);
			}
		});	

	}

	
	
	
//POBIERAMY LIST� POI I UAKTUALNIAMY LISTVIEW
	
	
	/**
	 * Background Async Task to Load all tracks under one album
	 * */
	class LoadPois extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(RoutesPoiListActivity.this);
			pDialog.setMessage("Pobieram list� POI...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * POBIERAMY JSON POI I PARSUJEMy
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			
			// post route id as GET parameter
			params.add(new BasicNameValuePair(TAG_ID, route_id));

			// getting JSON string from URL
			String json = jsonParser.makeHttpRequest(URL_POIS, "GET", params);

			try {
				JSONObject jObj = new JSONObject(json);
				
				if (jObj != null) {
					String route_id = jObj.getString(TAG_ID);
					route_name = jObj.getString(TAG_ROUTE_NAME);
					pois = jObj.getJSONArray(TAG_POIS);
					

					if (pois != null) {
						// looping through All POI
						for (int i = 0; i < pois.length(); i++) {
							JSONObject c = pois.getJSONObject(i);

							// Storing each json item in variable
							String poi_id = c.getString(TAG_ID);
							// POI no - increment i value
							String poi_no = String.valueOf(i + 1);
							String name = c.getString(TAG_POI_NAME);

							// creating new HashMap
							HashMap<String, String> map = new HashMap<String, String>();

							// adding each child node to HashMap key => value
							map.put("route_id", route_id);
							map.put(TAG_ID, poi_id);
							map.put("poi_no", poi_no + ".");
							map.put(TAG_POI_NAME, name);

							// adding HashList to ArrayList
							poiList.add(map);
						}
					} else {
						Log.d("Albums: ", "null");
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}
		
//UAKTUALNIAMY LIST� POI

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all tracks
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					RoutesPoiListActivity.this.adapter = new SimpleAdapter(
						RoutesPoiListActivity.this, poiList, R.layout.list_item_pois, 
						new String[] { "route_id", TAG_ID, "poi_no", TAG_POI_NAME }, 
						new int[] { R.id.route_id, R.id.poi_id, R.id.poi_no, R.id.poi_name }
					);
					
					// updating listview
					setListAdapter(adapter);
					// Change Activity Title with Album name
					setTitle(route_name);
				}
				
			});

		}

	}
}