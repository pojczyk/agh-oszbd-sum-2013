package com.ojczyk.tourmap;



import java.util.ArrayList;
import java.util.Arrays;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FindPoi extends Activity implements OnClickListener  {
	
	private ListView list_poiList; 
	private ArrayAdapter<String> listAdapter; 
	private Button btn_back;
	
	VehicleAdapter myadapter;
	
	ArrayList<String> poiList = new ArrayList<String>();
	AlertDialog.Builder alertDialogBuilder;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_poi);
		
		//PRZYCISKI
		/*btn_back = (Button) findViewById(R.id.back);
		btn_back.setOnClickListener(this);*/
		
		
		//LISTA POI
		list_poiList = (ListView) findViewById(R.id.poiList);
	    // Create and populate a List of planet names.  
	    String[] poi = new String[] { 
	    		"Rynek G��wny", "Ko�ci� Mariacki", "Katedra Wawelska", 
	    		"Zamek Kr�lewski", "Sukiennice", "Kazimierz", "Brama Floria�ska", 
	    		"Kopiec Ko�ciuszki", "Muzeum Etnograficzne", "Muzeum Narodowe",
	    		"Wie�a Ratuszowa","Stara Synagoga", "Dom Jana Matejki"}; 
	    
	    ArrayList<String> poiList = new ArrayList<String>();  
	    poiList.addAll( Arrays.asList(poi) );  
	    
        myadapter = new VehicleAdapter(FindPoi.this, poiList);
        
        //listView.setSelector( R.drawable.list_selector);
        
        list_poiList.setAdapter(myadapter);
        
        
	}
	
	//if you want to add functionality to your objects in the rows, you need to do it in your adapter class.
	//Now we are going to let the user delete a row from the list buy clicking on the button (ld. lent)
	 
    public class VehicleAdapter extends BaseAdapter{
    
    	public String title[];
    	public Activity context;
    	public LayoutInflater inflater;
    	
    	ArrayList<String> listItem = new ArrayList<String>();
    	
    	public VehicleAdapter(Activity context, ArrayList<String> listItem) {
    		super();
    		
    		this.context = context;
    		this.listItem = listItem;
    	    this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	    
    	}
    	
    	@Override
    	public int getCount() {
    		// TODO Auto-generated method stub
    		return listItem.size();
    	}

    	@Override
    	public Object getItem(int position) {
    		// TODO Auto-generated method stub
    		return null;
    	}

    	@Override
    	public long getItemId(int position) {
    		// TODO Auto-generated method stub
    		return 0;
    	}

    	public class ViewHolder{
    		TextView txtName;
    		Button btn;
    		RelativeLayout row;
    	}

    	@Override
    	public View getView(final int position, View convertView, ViewGroup parent) {
    		// TODO Auto-generated method stub

    		final ViewHolder holder;
    		
    		if(convertView==null){
    			
    			holder = new ViewHolder();
    			convertView = inflater.inflate(R.layout.list_item, null);
    			
    			holder.txtName = (TextView) convertView.findViewById(R.id.textView);
    			holder.btn = (Button) convertView.findViewById(R.id.button);
    			holder.row = (RelativeLayout) convertView.findViewById(R.id.lineItem);
    			
    			convertView.setTag(holder);
    		}
    		else
    			holder=(ViewHolder)convertView.getTag();

    		holder.txtName.setText(listItem.get(position));
    		
    		
    		//obs�uga przycisk�w na li�cie
			holder.btn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
				 	Toast.makeText(getApplicationContext(), "Przechodzisz do mapy. ", Toast.LENGTH_SHORT).show();
				}
			});
    		
			
			holder.row.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Toast.makeText(getApplicationContext(), "Przechodzisz do szczeg��w " + holder.txtName.getText(), Toast.LENGTH_SHORT).show();					
				}
			});
			
    		return convertView;
    		
    	}
	
    }
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tour, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.back:
	        	Intent addintent = new Intent(FindPoi.this,MainActivity.class);
				startActivity(addintent);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
    @Override
	public void onClick(View view) {
    	/*switch (view.getId()) {
        case R.id.back:
        	Intent addintent = new Intent(FindPoi.this,TourActivity.class);
			startActivity(addintent);
        	break;
    	}*/
	}
	
	

}
