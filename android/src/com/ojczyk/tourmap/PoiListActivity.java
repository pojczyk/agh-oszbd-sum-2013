package com.ojczyk.tourmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Filter;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.ojczyk.tourmap.helper.AlertDialogManager;
import com.ojczyk.tourmap.helper.ConnectionDetector;
import com.ojczyk.tourmap.helper.JSONParser;
import com.ojczyk.tourmap.MapActivity;


public class PoiListActivity extends ListActivity {
	
	
	
//PARAMETRY JSON I PO��CZENIA
	
	// Connection detector
	ConnectionDetector cd;
	
	// Alert dialog manager
	AlertDialogManager alert = new AlertDialogManager();
	
	// Progress Dialog
	private ProgressDialog pDialog;

	// Creating JSON Parser object
	JSONParser jsonParser = new JSONParser();

	ArrayList<HashMap<String, String>> poiList;

	// POI JSONArray
	JSONArray pois = null;
	
	MapActivity marker = new MapActivity();
	
	private ListView list_poiList; 
	
	
//FILTROWANIE LISTY
	//ADAPTER DO WYSZUKIWANIA I ZA�ADOWANIA LISTY
	VehicleAdapter adapter;
	// POLE SEARCH EditText
    EditText inputSearch;
	
	
	// POI JSON url
	private static final String URL_POIS = "http://ojczyk.nstrefa.pl/tour_map/poisList.php";

	// ALL JSON node names
	private static final String TAG_ID = "id";
	private static final String TAG_NAME = "name";
	private static final String LISTA_POI = "Lista POI";
	private static final String TAG_LAT = "lat";
	private static final String TAG_LNG = "lng";
	
	
//PODCZAS TWORZENIA

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//USTAWIAMY LAYER
		setContentView(R.layout.activity_routes);
		
		
//DETEKTOR PO��CZENIA Z INTERNETEM
		cd = new ConnectionDetector(getApplicationContext());
		 
        // SPRAWDZAMY PO��CZENIE INTERNETOWE
        if (!cd.isConnectingToInternet()) {
            //NIE UDA�O PO��CZYC SI� Z INTERNETEM
            alert.showAlertDialog(PoiListActivity.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }
        
        
		// HASHMAP DLA LISTVIEW
        poiList = new ArrayList<HashMap<String, String>>();

		// �ADUJEMY ALBUMY Z JSON
		new LoadAlbums().execute();
		
		// POBIERAMY LISTVIEW
		ListView lv = getListView();
		
		
//FILTROWANIE LISTVIEW
	    inputSearch = (EditText) findViewById(R.id.inputSearch);
	    inputSearch.setHint("znajd� POI...");
	    
		inputSearch.addTextChangedListener(new TextWatcher() {
		     
		    @Override
		    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
		        // When user changed the Text
		    	PoiListActivity.this.adapter.getFilter();
		    	//PoiListActivity.this.adapter.getFilter().filter(cs);
		    }
		     
		    @Override
		    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		        // TODO Auto-generated method stub
		    }
		     
		    @Override
		    public void afterTextChanged(Editable arg0) {
		        // TODO Auto-generated method stub                         
		    }
		});
	}
	

	/**
	 * �ADUJEMY WSZYSTKIE POI POPRZES HTTP REQUEST
	 * */
	class LoadAlbums extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(PoiListActivity.this);
			pDialog.setMessage("Pobieram list� POI ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * POBIERAMY JSON POI
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			// getting JSON string from URL
			String json = jsonParser.makeHttpRequest(URL_POIS, "GET", params);

			try {				
				pois = new JSONArray(json);
				
				if (pois != null) {
					// looping through All routes
					for (int i = 0; i < pois.length(); i++) {
						
						JSONObject c = pois.getJSONObject(i);

						// Storing each json item values in variable
						String id = c.getString(TAG_ID);
						String name = c.getString(TAG_NAME);
						String poi_no = String.valueOf(i + 1);
						String lat = c.getString(TAG_LAT);
						String lng = c.getString(TAG_LNG);
						
						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_ID, id);
						map.put("poi_no", poi_no + ".");
						map.put(TAG_NAME, name);
						map.put(TAG_LAT, lat);
						map.put(TAG_LNG, lng);

						// adding HashList to ArrayList
						poiList.add(map);
					}
				}else{
					//Log.d("Routes: ", "null");
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}
		
		
		
//UAKTUALNIAMY LIST� TRAS

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all albums
			pDialog.dismiss();
			
			// UPDATE UI
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * UPDATE LISTVIEW DANYMI Z JSON
					 * */
					adapter = new VehicleAdapter(PoiListActivity.this, poiList);
					
					// UPDATE LISTVIEW
					setListAdapter(adapter);
					setTitle(LISTA_POI);
				}
			});

		}

	}
	
	
	
	
	
	
	
//CUSTOM ADAPTER
	public class VehicleAdapter extends BaseAdapter implements Filterable{
	    
    	public Activity context;
    	public LayoutInflater inflater;
    	
    	ArrayList<HashMap<String,String>> listItem = new ArrayList<HashMap<String,String>>();
    	
    	private ArrayList<HashMap<String, String>> originalData;
        private ArrayList<HashMap<String, String>> filteredData;
    	
    	
    	//KONSTRUKTOR
    	public VehicleAdapter(Activity context, ArrayList<HashMap<String,String>> listItem) {
    		super();
    		
    		this.context = context;
    		this.listItem = listItem;
    	    this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	    
    	    originalData = listItem;
    	    filteredData = listItem;
    	}
    	
    	@Override
    	public int getCount() {
    		// TODO Auto-generated method stub
    		return filteredData.size();
    	}

    	@Override
    	public Object getItem(int position) {
    		
    		return filteredData.get(position);
    		
    	}

    	@Override
    	public long getItemId(int position) {

    		return position;
    	}

    	public class ViewHolder{
    		TextView name, objectId, lat, lng;
    		Button btn;
    		RelativeLayout row;
    	}

    	@Override
    	public View getView(final int position, View convertView, ViewGroup parent) {
    		// TODO Auto-generated method stub

    		final ViewHolder holder;
    		
    		if(convertView==null){
    			
    			holder = new ViewHolder();
    			convertView = inflater.inflate(R.layout.list_item, null);
    			
    			//POBIERAMY ELEMENTY Z LISTY
    			holder.name = (TextView) convertView.findViewById(R.id.textView);
    			holder.objectId = (TextView) convertView.findViewById(R.id.object_id);
    			holder.lat = (TextView) convertView.findViewById(R.id.lat);
    			holder.lng = (TextView) convertView.findViewById(R.id.lng);
    			holder.btn = (Button) convertView.findViewById(R.id.button);
    			holder.row = (RelativeLayout) convertView.findViewById(R.id.lineItem);
    			
    			
    			convertView.setTag(holder);
    		}else{
    			holder=(ViewHolder)convertView.getTag();
    		}
    		
    		holder.name.setText(poiList.get(position).get(TAG_NAME));
    		holder.objectId.setText(poiList.get(position).get(TAG_ID));
    		holder.lat.setText(poiList.get(position).get(TAG_LAT));
    		holder.lng.setText(poiList.get(position).get(TAG_LNG));
    		
    		
    		//obs�uga przycisk�w na li�cie
			holder.btn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = new Intent(getApplicationContext(), MapActivity.class);
					
					
					String lat = holder.lat.getText().toString();
					String lng = holder.lng.getText().toString();
					String name = holder.name.getText().toString();
					
					i.putExtra("lat", lat);	
					i.putExtra("lng", lng);	
					i.putExtra("name", name);
					
					startActivity(i);
				}
			});
			
			
			holder.row.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//GDY WYBIERZEMY POJEDY�CZ� TRAS� ZOSTANIE WCZYTANA LISTA POI
					Intent i = new Intent(getApplicationContext(), PoiActivity.class);
					
					//WYSY�AMY ID TRASY DO LISTY POI ABY UZYSKAC LIST� POI DLA TEJ TRASY
					String route_id = holder.objectId.getText().toString();
					i.putExtra("poi_id", route_id);				
					
					startActivity(i);				
				}
			});
    		return convertView;
    	}
    	
    	@Override
        public android.widget.Filter getFilter() {
            return null;
        }
    }
	
	
	
	
	
}