package com.ojczyk.tourmap;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;

public class CopyOfMapActivity extends FragmentActivity {
	
	private GoogleMap googleMap;
    private int mapType = GoogleMap.MAP_TYPE_NORMAL;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_activity);
		
//POBRANIE MAPY DO ZMIENNEJ
		
		FragmentManager fragmentManager = getSupportFragmentManager();
        SupportMapFragment mapFragment =  (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        googleMap = mapFragment.getMap();
        
//DODANIE MARKERA
        
        //WSPӣRZ�DNE
        LatLng sfLatLng = new LatLng(50.046766, 20.004863);
        //OPCJE MARKERA
        googleMap.addMarker(
        		new MarkerOptions()
                .position(sfLatLng)
                .title("Krak�w")
                .snippet("Nareszcie sie uda�o hehe :)")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        
//WYCENTROWANIE KAMERY
        
        //WSPӣRZ�DNE
        LatLng cameraLatLng = sfLatLng;
        //ZOOM
        float cameraZoom = 10;
        
        if(savedInstanceState != null){
        	//TYP MAPY
            mapType = savedInstanceState.getInt("map_type", GoogleMap.MAP_TYPE_NORMAL);

            double savedLat = savedInstanceState.getDouble("lat");
            double savedLng = savedInstanceState.getDouble("lng");
            cameraLatLng = new LatLng(savedLat, savedLng);

            cameraZoom = savedInstanceState.getFloat("zoom", 10);
        }
        //WYKONUJEMY AKCJ�
        googleMap.setMapType(mapType);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cameraLatLng, cameraZoom));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
