package com.ojczyk.tourmap;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.ojczyk.tourmap.helper.AlertDialogManager;
import com.ojczyk.tourmap.helper.ConnectionDetector;
import com.ojczyk.tourmap.helper.JSONParser;

public class PoiActivity extends Activity {
	
	
	
	
	// Connection detector
	ConnectionDetector cd;
	
	// Alert dialog manager
	AlertDialogManager alert = new AlertDialogManager();
	
	// Progress Dialog
	private ProgressDialog pDialog;

	// Creating JSON Parser object
	JSONParser jsonParser = new JSONParser();

	// tracks JSONArray
	JSONArray albums = null;
	
	// POI id
	String poi_id = null;
	
	String poi_name, duration, content;

	// single song JSON url
	// GET parameters album, song
	private static final String URL_SONG = "http://ojczyk.nstrefa.pl/tour_map/poi.php";

	// ALL JSON node names
	private static final String TAG_NAME = "name";
	private static final String TAG_CONTENT = "content";
	
	
	
	
	
	
	
//PODCZAS TWORZENIA
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_poi);
		
//DETEKTOR PO��CZENIA Z INTERNETEM
		cd = new ConnectionDetector(getApplicationContext());
		 
		// SPRAWDZAMY PO��CZENIE INTERNETOWE
        if (!cd.isConnectingToInternet()) {
        	//NIE UDA�O PO��CZYC SI� Z INTERNETEM
            alert.showAlertDialog(PoiActivity.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            return;
        }
        
        // Get poi id
        Intent i = getIntent();
        poi_id = i.getStringExtra("poi_id");
        
        // calling background thread
        new LoadSinglePoi().execute();
	}
	
	
	
	
	
	
	
	
	
	/**
	 * �ADUJEMY POI POPRZEZ HTTP REQUEST
	 * */
	class LoadSinglePoi extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(PoiActivity.this);
			pDialog.setMessage("Pobieram szczeg�y POI ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting song json and parsing
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			
			// post album id, song id as GET parameters
			params.add(new BasicNameValuePair("poi", poi_id));

			// getting JSON string from URL
			String json = jsonParser.makeHttpRequest(URL_SONG, "GET", params);

			// Check your log cat for JSON reponse
			//Log.d("Single Track JSON: ", json);

			try {
				JSONObject jObj = new JSONObject(json);
				if(jObj != null){
					poi_name = jObj.getString(TAG_NAME);
					content = jObj.getString(TAG_CONTENT);
				}			

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		
		
		
		
		
		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting song information
			pDialog.dismiss();
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					
					TextView txt_song_name = (TextView) findViewById(R.id.poi_name);
					TextView txt_content = (TextView) findViewById(R.id.content);
					
					// displaying song data in view
					txt_song_name.setText(poi_name);
					txt_content.setText(Html.fromHtml(content));
					
					// Change Activity Title with Song title
					setTitle(poi_name);
				}
			});

		}

	}
}
