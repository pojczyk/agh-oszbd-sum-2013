package com.ojczyk.tourmap;


import com.ojczyk.tourmap.helper.AlertDialogManager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.view.View.OnClickListener;


public class MainActivity extends Activity implements OnClickListener {
	
//DEKLARACJE BUTTON�W
	private Button btn_map, btn_route, btn_poi, btn_own_route;
	// Alert dialog manager
	AlertDialogManager alert = new AlertDialogManager();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//USTAWIAMY KT�RY LAY MA WSZYTA� SI� PODCZAS STARTU APLIKACJI
		setContentView(R.layout.main);
		
//DODANIE LISTENER�W DO BUTTONOW
		/*btn_find = (Button) findViewById(R.id.findPoi);
		btn_find.setOnClickListener(this);*/
		
		btn_map = (Button) findViewById(R.id.showMap);
		btn_map.setOnClickListener(this);
		
		btn_route = (Button) findViewById(R.id.findRoute);
		btn_route.setOnClickListener(this);
		
		btn_poi = (Button) findViewById(R.id.findPoi);
		btn_poi.setOnClickListener(this);
		
		btn_own_route = (Button) findViewById(R.id.ownRoute);
		btn_own_route.setOnClickListener(this);
	}
	
	
	public void onClick(View view) {
		
//SWITCH WCZYTUJ�CY ODPOWIEDNIE LAYERY
		switch (view.getId()) {
			/*case R.id.findPoi:
				Intent showFindPoi = new Intent(MainActivity.this,FindPoi.class);
				startActivity(showFindPoi);
				break;*/
				
			case R.id.showMap:
				Intent showMap = new Intent(MainActivity.this,RoutesMapActivity.class);
				startActivity(showMap);
				break;
				
			case R.id.findRoute:
				Intent showRoutes = new Intent(MainActivity.this,RoutesActivity.class);
				startActivity(showRoutes);
				break;
				
			case R.id.findPoi:
				Intent showPoi = new Intent(MainActivity.this,PoiListActivity.class);
				startActivity(showPoi);
				break;
				
			case R.id.ownRoute:
				alert.showAlertDialog(MainActivity.this, "Informacja","Ta opcja jest jeszcze niedost�pna", false);
				break;
				
			default:
				break;
		}

	}

}
