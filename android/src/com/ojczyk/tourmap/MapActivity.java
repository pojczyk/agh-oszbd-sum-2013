package com.ojczyk.tourmap;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ojczyk.tourmap.helper.AlertDialogManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.widget.SimpleAdapter;

public class MapActivity extends FragmentActivity {
	
	private GoogleMap googleMap;
    private int mapType = GoogleMap.MAP_TYPE_NORMAL;
    
    String lat, lng, name;
    double latDouble, lngDouble;
    
    AlertDialogManager alert = new AlertDialogManager();
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_activity);
		
//POBRANIE MAPY DO ZMIENNEJ
		
		FragmentManager fragmentManager = getSupportFragmentManager();
        SupportMapFragment mapFragment =  (SupportMapFragment) fragmentManager.findFragmentById(R.id.map);
        googleMap = mapFragment.getMap();
        
        
     // POBIERAMY DANE PUNKTU Z POSTA
        Intent i = getIntent();
        lat = i.getStringExtra("lat");
        lng = i.getStringExtra("lng");
        name = i.getStringExtra("name");
        
        latDouble = Double.parseDouble(lat);
        lngDouble = Double.parseDouble(lng);
        
        addMarker(latDouble, lngDouble, name, "Opis");
        
        
        
        
        //nearPlaces = (PlacesList) i.getSerializableExtra("near_places");
        
/*
        
//WYCENTROWANIE KAMERY ZE STANU SAVED
        LatLng sfLatLng = new LatLng(50.046766, 20.004863);
        //WSPӣRZ�DNE
        LatLng cameraLatLng = sfLatLng;
        //ZOOM
        float cameraZoom = 10;
        
        if(savedInstanceState != null){
        	//TYP MAPY
            mapType = savedInstanceState.getInt("map_type", GoogleMap.MAP_TYPE_NORMAL);

            double savedLat = savedInstanceState.getDouble("lat");
            double savedLng = savedInstanceState.getDouble("lng");
            cameraLatLng = new LatLng(savedLat, savedLng);

            cameraZoom = savedInstanceState.getFloat("zoom", 10);
        }
        //WYKONUJEMY AKCJ�
        googleMap.setMapType(mapType);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cameraLatLng, cameraZoom));*/
	}
	
	
	
//DODANIE MARKERA
	public void addMarker(double lat, double lng, String name, String content){
		float cameraZoom = 10;
        //WSPӣRZ�DNE
        LatLng sfLatLng = new LatLng(lat, lng);
        //OPCJE MARKERA
        googleMap.addMarker(
        		new MarkerOptions()
                .position(sfLatLng)
                .title(name)
                .snippet(content)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        
        //WYKONUJEMY AKCJ�
        googleMap.setMapType(mapType);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sfLatLng, cameraZoom));
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
