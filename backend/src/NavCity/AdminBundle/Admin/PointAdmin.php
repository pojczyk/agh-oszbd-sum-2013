<?php

namespace NavCity\AdminBundle\Admin;

use Oh\GoogleMapFormTypeBundle\Form\Type\GoogleMapType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @file
 * @ingroup     Main
 * @brief       Brief
 *
 * More description..
 *
 * @since       11.05.13 10:21 GMT+2
 * @author      ... Team <...@team.tld>
 * @version     1
 * @date        11.05.13 10:21 GMT+2
 */

/**
 * Class PointAdmin
 *
 * @package NavCity\AdminBundle\Admin
 */
class PointAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $latlngOptions = array(
            'map_width'   => 1000,
            'map_height'  => 300,
            'default_lat' => 50.067893,
            'default_lng' => 19.912779,
        );
        $formMapper
            ->add('type')
            ->add('city')
            ->add('name')
            ->add('address', null, array('attr' => array('class' => 'address-box',),))
            ->add('latlng', new GoogleMapType(), $latlngOptions, array('type' => 'oh_google_maps',))
            ->add('image')
            ->add('description')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('city')
            ->add('type')
            ->add('address')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('city')
            ->add('type')
            ->add('address')
        ;
    }
}
