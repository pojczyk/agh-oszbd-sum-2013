<?php

namespace NavCity\JsonBundle\Controller;

use NavCity\ApiBundle\Entity\Point;
use NavCity\ApiBundle\Entity\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as TemplateRoute;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class RouteController extends Controller
{
    /**
     * @TemplateRoute(path="/route/{id}", requirements={"id" = "\d+"})
     * @Template()
     */
    public function indexAction($id)
    {
        return array('route' => $this->findRoute($id),);
    }

    /**
     * @TemplateRoute("/routes")
     * @Template()
     */
    public function listAction()
    {
        return array('routes' => $this->findRoutes(),);
    }

    /**
     * @param integer $id
     *
     * @return array|null
     */
    private function findRoute($id)
    {
        /** @var Route $route */
        if (!$route = $this->getDoctrine()->getRepository('NavCityApiBundle:Route')->find($id)) {
            return null;
        }

        $pois = array();
        /** @var Point $poi */
        foreach ($route->getPoint() as $poi) {
            $pois[] = array(
                'id'   => $poi->getId(),
                'name' => $poi->getName(),
                'lat'  => $poi->getLat(),
                'lng'  => $poi->getLng(),
            );
        }

        return array(
            'id'         => $route->getId(),
            'route_name' => $route->getName() ? : '',
            'pois'       => $pois,
        );
    }

    /**
     * @return array
     */
    private function findRoutes()
    {
        $routes = array();
        /** @var Route $route */
        foreach ($this->getDoctrine()->getRepository('NavCityApiBundle:Route')->findAll() as $route) {
            $routes[] = array(
                'id'        => $route->getId(),
                'name'      => $route->getName(),
                'poi_count' => $route->getPoint()->count(),
            );
        }

        return $routes;
    }
}
