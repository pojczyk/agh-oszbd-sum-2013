<?php

namespace NavCity\ApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * LogRecord
 *
 * @ORM\Table(name="log_record")
 * @ORM\Entity
 */
class LogRecord
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=false)
     */
    private $message;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \LogSeverity
     *
     * @ORM\ManyToOne(targetEntity="LogSeverity")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="severity_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $severity;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return LogRecord
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     *
     * @return LogRecord
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set severity
     *
     * @param LogSeverity $severity
     *
     * @return LogRecord
     */
    public function setSeverity(LogSeverity $severity = null)
    {
        $this->severity = $severity;

        return $this;
    }

    /**
     * Get severity
     *
     * @return LogSeverity
     */
    public function getSeverity()
    {
        return $this->severity;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('[%s:%s] %s', $this->severity, $this->createdAt, $this->message);
    }
}
