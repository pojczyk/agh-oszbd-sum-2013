<?php

namespace NavCity\ApiBundle\DataFixtures\ORM;

/**
 * @file
 * @ingroup     Main
 * @brief       Brief
 *
 * More description..
 *
 * @since       7.06.13 11:01 GMT+2
 * @author      ... Team <...@team.tld>
 * @version     1
 * @date        7.06.13 11:01 GMT+2
 */
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NavCity\ApiBundle\Entity\Point;

/**
 * Class LoadPointData
 *
 * @package NavCity\ApiBundle\DataFixtures\ORM
 */
class LoadPointData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $point1 = new Point();
        $point1->setAddress('Rynek Główny 3')
            ->setCity($this->getReference('city-krakow'))
            ->setDescription(null)
            ->setImage(null)
            ->setLat(50.061657198278326)
            ->setLng(19.937245845794678)
            ->setName('Sukiennice')
            ->setType($this->getReference('point-type-poi'));
        $manager->persist($point1);

        $point2 = new Point();
        $point2->setAddress('Kawiory 10')
            ->setCity($this->getReference('city-krakow'))
            ->setDescription(null)
            ->setImage(null)
            ->setLat(50.06769659025712)
            ->setLng(19.91327709101563)
            ->setName('AGH KI')
            ->setType($this->getReference('point-type-poi'));
        $manager->persist($point2);

        $point3 = new Point();
        $point3->setAddress('kapitana Mieczysława Medweckiego 3')
            ->setCity($this->getReference('city-krakow'))
            ->setDescription(null)
            ->setImage(null)
            ->setLat(50.07206310387079)
            ->setLng(19.80156898498535)
            ->setName('Lotnisko Balice')
            ->setType($this->getReference('point-type-poi'));
        $manager->persist($point3);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}
