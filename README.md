# Projekt NavCity

## Instrukacja obslugi

### Wymagania wstepne

 1. Nalezy uruchomic eksploratora plikow i przejsc pod:
    `C:\Users\oszbd\Moje Aplikacje`
    W katalogu znajduja sie:
      * `adt` - Andorid Development Tools (Eclipse + SDK + Manager),
      * `net.navcity` - projekt podlegajacy rewizji,
      * `php-5.4.15` - binarki PHP,
      * `workspace` - domyslny katalog roboczy Eclipse ADT,
      * `bituser.txt` - adres e-mail wykorzystany do rejestracji na bitbucket.org (uzytkownik adresu jest rowniez uzytkownikiem na BB a caly adres e-mail jest haslem),
      * `README.md` - ten plik,
 2. Nalezy otworzyc menu kontektstowe w katalogu domowym aplikacji i wybrac "Git Bash".

### Aktualizacja aplikacji webowej

 1. W oknie konsoli nalezy przejsc do podkatalogu `net.navcity` uzywajac `cd net.navcity`.
 2. W katalogu `net.navcity` nalezy wykonac polecenie `git pull` (uaktualnienie plikow do najnowszej wersji).
 3. Aby dokonczyc aktualizacji kodu nalezy teraz wywolac z wiersza polecen: `php composer.phar update`.
    Spowoduje to, ze wszystkie nowo zdefiniowane zaleznosci w `composer.json` zostana pobrane.
		Dodatkowo `composer` upewnia sie, ze mamy wygenerowane pliki, cache jest na miejscu oraz zasowby CSS, JS lub obrazki sa dostepne z poziomu HTTP (katalog /web).
 4. Nastepnie nalezy przejsc do katalogu `backend` i wykonac `app/console doctrine:migrations:migrate --no-interaction`.

### Uruchamianie serwera

 1. Nalezy uruchomic nowe okno konsoli w katalogu domowym aplikacji i przejsc do katalogu `net.navcity/backend/web`.
 2. Tam nalezy uruchomic komende `php -S net.navcity:80`. Dzieki temu bedziemy teraz w stanie uruchomic aplikacje webowa wpisujac w przegladarce http://net.navcity/

### Wymuszona aktualizacja struktury bazy danych

 1. Nalezy uruchomic z `Start` konsole MySQL.
 2. Haslo dla root to root.
 3. Nastepnie nalezy uzyc komendy `USE navcity`.
 4. Celem usuniecia wszystkich tabel w jednym rzucie nalezy wykonac:
    `DROP IF EXISTS DATABASE navcity; CREATE DATABASE navcity; USE navcity;`
 5. Teraz wracamy do konsoli "Git Bash" i upewniamy sie komenda `pwd`, ze jestesmy w:
    `/C/Users/oszbd/Moje Aplikacje/net.navcity/backend`
 6. W celu utworzenia schematu wykonujemy:
    `app/console doctrine:schema:create`
    Aby upewnic sie, ze wszystko jest OK mozemy przejsc do konsoli MySQL i wykonac zapytanie:
    `SHOW TABLES;`
 6. Tworzymy super administratora uzywajac komendy:
    `app/console fos:user:create --super-admin super super@super super`
    Celem uzyskania informacji o powyzszej komendzie nalezy wykonac:
    `app/console help fos:user:create`
 7. Aby dodac zalazek danych nalezy wykonac:
    `app/console doctrine:fixtures:load --append`

## Interfejs

Aplikacja webowa z zalozenia miala byc wykorzystana jedynie do zarzadzania danymi.

Dodalismy rowniez prezentowanie ostatnio dodanego POI + N najblizszych punktow (domyslnie 1 + 1).
Aby zobaczyc na mapie punkty nalezy przejsc pod adres:

> http://net.navcity/app_dev.php

Aby zmienic liczbe punktow na mapie (1 najnowszy + do 4 sasiadow) nalezy przejsc pod adres:

> http://net.navcity/app_dev.php/5

Aby zobaczyc interfejs administracyjny nalezy przejsc pod:

> http://net.navcity/app_dev.php/admin/dashboard

W momencie oddawania obrazu ostatnia sesja przegladarki Chrome posiada otwarte przykladowe linki.

